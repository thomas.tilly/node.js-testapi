const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://thomas:21d9371ff00@cluster0-bttcp.azure.mongodb.net/test?retryWrites=true&w=majority";
const client = new MongoClient(uri, {
    useNewUrlParser: true
});
client.connect(async err => {
    const collection = client.db("dbexo1").collection("dates");
    let r = await collection.insertOne({
        date: (new Date()).toString()
    });
    const document = await collection.find({}).toArray();
    console.log('docs:', document)
    client.close();
    console.log('connexion fermée')
});